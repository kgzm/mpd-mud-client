const Rx = require('rxjs');
const { toArray } = require('rxjs/operators');
const { marbles } = require('rxjs-marbles/jest');
const { EventEmitter } = require('events');

const { createPlayerListener, socketObservable } = require('../server.js');

describe('Server', () => {
  let fakeSocket = (remoteAddress, remotePort) => {
    return Object.assign(new EventEmitter(), {
      remoteAddress,
      remotePort,
      command(data)  { this.emit('data', data ) },
      write: jest.fn(() => true),
      end: jest.fn(() => true),
      close: jest.fn(() => true),
      destroy: jest.fn(() => true),
    });
  }

  let fakeServer = () => {
    let acceptConnection;
    let server = {
      listen: jest.fn(() => true),
      close: jest.fn(() => true),
    };
    let createServer = jest.fn((handler) => {
      acceptConnection = (socket) => handler(socketObservable(socket));
      return server;
    });
    let broadcast$ = new Rx.Subject();
    let server$ = createPlayerListener(1234, broadcast$, createServer);
    replay$ = new Rx.ReplaySubject(20);
    server$.subscribe(replay$);
    return {
      server,
      acceptConnection,
      server$: replay$,
      broadcast$,
    }
  }

  test("listens", async () => {
    let { server, broadcast$, server$ } = fakeServer();
    broadcast$.complete();
    let results = await server$.pipe(toArray()).toPromise()
    expect(server.listen).toHaveBeenCalled();
    expect(results).toEqual(expect.arrayContaining(
      [{ topic: 'server', listening: 1234 }]
    ))
  });

  test("closes when no one cares", async () => {
    let { server, broadcast$, server$ } = fakeServer();
    broadcast$.complete();
    let results = await server$.pipe(toArray()).toPromise()
    expect(server.close).toHaveBeenCalled();
    expect(results).toEqual(expect.arrayContaining(
      [{ topic: 'server', closing: 1234 }]
    ))
  });

  test("reports connections and disconnections", async () => {
    let { broadcast$, server$, acceptConnection } = fakeServer();
    let socket = fakeSocket('1111', 5555);
    acceptConnection(socket);
    socket.emit('end');
    broadcast$.complete();
    let results = await server$.pipe(toArray()).toPromise()
    expect(results).toEqual(expect.arrayContaining(
      [expect.objectContaining({ connected: '1111:5555' }),
       expect.objectContaining({ disconnected: '1111:5555' })]
    ))
  });

  test.skip("drops clients when no one is talking to the server",  () => {
    // I have no idea how to test this.
    // Since we're not using a reals erver there's no machinery to make sure the connections get cleaned up.
    // Further, node's net.Server does not clean up the connections when it is closed!
    // I also need to add a kick function to drop clients.
    // Both to remove malicious clients or in the case of a quit command.
    let { broadcast$, server$, acceptConnection } = fakeServer();
    let socket = fakeSocket('1111', 5555);
    acceptConnection(socket);
    broadcast$.complete();
    expect(socket.close).toHaveBeenCalled();
  });

  test("receives input from sockets", async () => {
    let { broadcast$, server$, acceptConnection } = fakeServer();
    let socket = fakeSocket();
    acceptConnection(socket);
    socket.command("I'm tough.");
    broadcast$.complete();
    let results = await server$.pipe(toArray()).toPromise()
    expect(results).toEqual(expect.arrayContaining(
      [expect.objectContaining({ topic: 'interaction', command: "I'm tough." })]
    ))
  });

  test("writes command feedback to the socket", () => {
    let { broadcast$, acceptConnection } = fakeServer();
    let socket = fakeSocket('1111', 5555);
    acceptConnection(socket);
    let msg = { topic: 'feedback', id: '1111:5555', output: 'abcdefg' };
    broadcast$.next(msg);
    expect(socket.write).toHaveBeenCalledWith(msg.output);
    broadcast$.complete();
  });

  test("accepts state changes", async () => {
    let { server$, broadcast$, acceptConnection } = fakeServer();
    let socket = fakeSocket('1111', 5555);
    let stream$ = new Rx.Subject();
    acceptConnection(socket);
    let msg = {
      topic: 'control',
      state: {
        awake: true
      },
      id: '1111:5555',
      output: "You wake up."
    };
    broadcast$.next(msg);
    socket.command('yawn');
    broadcast$.complete();
    let results = await server$.pipe(toArray()).toPromise()
    expect(results).toEqual(expect.arrayContaining(
      [expect.objectContaining({ state: expect.objectContaining({ awake: true }) })]
    ));
  });

  test("writes mud output to socket", async () => {
    let { broadcast$, acceptConnection } = fakeServer();
    let socket = fakeSocket('1111', 5555);
    acceptConnection(socket);
    broadcast$.next({
      topic: 'control',
      state: {
        awake: true,
        authorized: true
      },
      id: '1111:5555',
      output: "you wake up."
    });
    let msg = { topic: 'mud', output: 'all action!' };
    broadcast$.next(msg);
    broadcast$.complete();
    expect(socket.write).toHaveBeenCalledWith(msg.output);
  });

  test("writes to all sockets", () => {
    let { broadcast$, acceptConnection } = fakeServer();
    let socket1 = fakeSocket('1111', 5555);
    let socket2 = fakeSocket('1234', 2222);
    let awaken = {
      topic: 'control',
      state: {
        awake: true,
        authorized: true
      },
      output: "you wake up."
    };
    let msg = { topic: 'mud', output: 'all action!' };
    acceptConnection(socket1);
    acceptConnection(socket2);
    broadcast$.next({ id: '1111:5555', ...awaken });
    broadcast$.next({ id: '1234:2222', ...awaken });
    broadcast$.next(msg);
    broadcast$.complete();
    expect(socket1.write).toHaveBeenCalledWith(msg.output);
    expect(socket2.write).toHaveBeenCalledWith(msg.output);
  });

  test("doesn't leak between sockets", () => {
    let { broadcast$, acceptConnection } = fakeServer();
    let socket1 = fakeSocket('1111', 5555);
    let socket2 = fakeSocket('1234', 2222);
    let awaken = {
      topic: 'control',
      state: {
        awake: true,
        authorized: true
      },
      output: "you wake up."
    };
    let msg = { topic: 'control', id: '1111:5555', output: 'all action!' };
    acceptConnection(socket1);
    acceptConnection(socket2);
    broadcast$.next({ id: '1111:5555', ...awaken });
    broadcast$.next({ id: '1234:2222', ...awaken });
    broadcast$.next(msg);
    broadcast$.complete();
    expect(socket1.write).toHaveBeenCalledWith(msg.output);
    expect(socket2.write).toHaveBeenCalledTimes(1);
  });
});
