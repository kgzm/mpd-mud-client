const Rx = require('rxjs');
const { filter, takeWhile, finalize, multicast, refCount } = require('rxjs/operators');
const net = require('net');

const socketObservable = (socket) => {
  return {
    socket,
    stream$: Rx.Observable.create((o) => {
      socket.on('data', (data) => {
        o.next({ command: data.toString().trimEnd() });
      })
      socket.on('end', () => {
        o.complete();
      })
      socket.on('close', () => {
        o.complete();
      })
      socket.on('error', (e) => {
        o.next({ error: e })
        o.complete();
      })
      return () => socket.end()
    })
  };
}

const makeServer = (socketHandler) => new net.Server(
  (socket) => socketHandler(socketObservable(socket))
);

let createPlayerListener = (port = 9000, broadcast$, createServer) => {
  let subject = new Rx.Subject()
  return Rx.Observable.create((o) => {
    let log = (msg) => {
      o.next({ topic: 'server', ...msg });
    };

    let server = createServer(({ socket, stream$ }) => {
      let id = `${socket.remoteAddress}:${socket.remotePort}`;
      log({ connected: id });

      let state = {
        alive: true,
        awake: false,
        authorized: false
      };

      stream$.pipe(
        finalize(() => {
          state.alive = false;
          log({ disconnected: id });
        })
      ).subscribe(({ command, end, error }) => {
        if (command) {
          o.next({ topic: 'interaction', id, state, command });
        } else if (error) {
          log({ error: id, details: e });
        }
      });

      broadcast$.pipe(
        takeWhile(() => state.alive),
        filter(({ topic, id: pid }) =>
          (topic == 'mud' && state.authorized && state.awake) ||
          (topic == 'feedback' && id == pid)
        )
      ).subscribe(({ output }) => socket.write(output));

      broadcast$.pipe(
        takeWhile(() => state.alive),
        filter(({ topic, id: pid }) => topic == 'control' && id == pid)
      ).subscribe(({ state: newState, output }) => {
        socket.write(output);
        state = { ...state, ...newState };
      });

    });
    server.listen(port);
    log({ listening: port });
    broadcast$.pipe(
      finalize(() => {
        log({ closing: port });
        o.complete();
        server.close();
      })
    ).subscribe(() => null);
  }).pipe(multicast(subject), refCount());
}

const createServer = (port = 9000, broadcast$) => createPlayerListener(port, broadcast$, makeServer);

module.exports = {
  socketObservable,
  createPlayerListener,
  createServer
};
