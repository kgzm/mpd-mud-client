const net = require('net');
const nedb = require('nedb-promises');
const Rx = require('rxjs');
const event = require('events')

const {
  MUD_HOST,
  MUD_PORT,
  MUD_USER,
  MUD_PASSWORD,
  MUD_ROOT_PASSWORD,
  PORT
} = process.env;

const Store = nedb.create('info.nedb');
const playerStream = new Rx.Subject();
const Players = new Set();

const Mud = {
  initialize() {
    console.log("Mud subscribing!");
    this.subscription = playerStream.subscribe(
      ({action, player}) => {
        let = reaction = 'player_' + action;
        this[reaction] && this[reaction](player);
      }, 
      (e) => {
        console.debug("playerstream error", e)
      },
      () => {
        console.debug("playerstream complete");
      }
    );
  },
  player_wakeup() {
    if(!this.ready) {
      this.connect();
    }
  },
  player_snooze() {
    let awake = Array.from(Players).filter(p => p.awake).length;
    if(!awake) {
      this.socket.end();
    }
  },
  connect() {
    let address = `${MUD_HOST}:${MUD_PORT}`;
    let socket = new net.createConnection(MUD_PORT, MUD_HOST);
    console.log("Connecting to ", address);
    socket.on('ready', () => {
      console.log("Connected to ", address);
      if(MUD_USER && MUD_PASSWORD) {
        this.send(`connect ${MUD_USER} ${MUD_PASSWORD}`);
      }
      this.ready = true;
    });
    socket.on('end', () => {
      this.ready = false;
    })
    socket.on('data', (data) => {
      //Immoral, improve immediately.
      let output = data.toString();
      Array.from(Players)
      .filter((p) => p.listening)
      .forEach((p) => p.notify(output));
    });
    this.socket = socket;
  },
  send(command) {
    console.log("sending", command);
    this.socket.write(command.trimEnd() + '\n');
  },
  ready: false
};

const Player = {
  new(socket) {
    let player = Object.create(Player);
    player.socket = socket;
    socket.on('data', (data) => {
      player.do(data.toString());
    });
    socket.on('close', () => {
      // TODO: Broadcast player leaving?
      Players.delete(this);
    });
    return player;
  },
  do(stuff) {
    // TODO: Clean up player.do
    let match = stuff.trim().match(/,([^\s]*)\s?(.*)$/);
    if(match) {
      let [,rawCommand,args] = match;
      let command = "cmd_" + rawCommand.toLowerCase();
      // Perform commmand.
      console.debug("do",{command, args});
      if(this.authenticated || command == "cmd_login") {
        if(this[command]) {
          this[command](args);
        } else {
          this.tell("I don't know how to do that.");
        }
      } 
    } else if(this.awake) {
      this.doOnMud(stuff);
    } else {
      this.tell("You're still asleep.");
    }
  },
  doOnMud(stuff) {
    let command = stuff;
    if(this.info.style) {
      command = stuff.replace(/^(say |")(.*)/, `:${this.info.style}, \"$2\"`);  
    }
    Mud.send(command);
  },
  cmd_users() {
    Store.find({username: {$exists: true} })
    .then((user) => {
      this.tell(JSON.stringify(user));
    });
  },
  cmd_login(args) {
    let [username, password] = args.split(' ');
    console.debug("login", {username, password}, MUD_ROOT_PASSWORD);
    if(username == "root" && password == MUD_ROOT_PASSWORD) {
      this.authenticated = true;
      this.root = true;
      this.tell("welcome root");
    } else {
      Store.findOne({username, password})
      .then((info) => {
        console.log("login", info);
        if(info) {
          this.info = info;
          this.authenticated = true;
          this.tell(`Welcome, ${username}, you are asleep...`);
        } else {
          this.tell("I don't know you.");
        }
      })
      .catch((e) => {
        console.debug(e);
        this.tell("I don't know you.");
      });
    }
  },
  cmd_create(args) {
    let [username, password] = args.split(' ');
    console.debug("create", {username, password});
    Store.findOne({username})
    .then((user) => {
      console.log(user);
      if(!user) {
        Store.insert({username, password})
        .then(() => this.tell(`Created ${username}`));
      } else if(user && this.root) {
        Store.update(user, {...user, password})
        .then(n => this.tell(`Repassed ${username}`));
      }
    });
  },
  cmd_style(style) {
    if(!style || style.length == 0) {
      this.tell("Try: ',style growls' ',style screams' or something else of the like.");
      this.tell(`Your current style is ${this.info.style}`);
      return;
    }
    this.info.style = style;
    Store.update({_id: this.info._id}, {...this.info, style})
    .then(() => this.tell("Style updated"));

  },
  cmd_wakeup() {
    this.wakeup();
  },
  cmd_snooze() {
    this.snooze();
  },
  act(action, opts = {}) {
    playerStream.next({
      action,
      player: this,
      ...opts
    });
  },
  wakeup() {
    this.awake = true;
    this.act('wakeup');
  },
  sleep() {
    this.awake = false;
    this.act('snooze');
  },
  notify(output) {
    if(this.socket) {
      this.socket.write(output);
    }
  },
  tell(output) {
    this.notify(output.trimEnd() + '\n');
  },
  get listening() {
    return this.authenticated && this.awake;
  },
  // Persist players in NeDB.
  info: {
    id: null,
    username: null,
    password: null,
    style: null,
  },
  socket: null,
  awake: false,
  authenticated: false,
}
const Server = {
  listen() {
    let server = new net.Server((socket) => {
      console.log("Received ", socket.remoteAddress, socket.remotePort);
      Players.add(Player.new(socket));
    });
    this.server = server;
    server.listen(PORT || 9000);
  }
}

Server.listen();
Mud.initialize();